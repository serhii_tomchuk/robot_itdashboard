__author__ = "Serhii Tomchuk"

import random

from helper.constants import SITE_DATA
from helper.parser import DashboardHandler
from helper.pdf_handler import PdfHandler
from helper.xlsx_handler import XlsxHandler

parser = DashboardHandler(
    base_url=SITE_DATA["BASE_URL"],
    prefix_base_url=SITE_DATA['prefix_base'],
    base_api_url=SITE_DATA["API"]["BASE_API"],
    agencies_prefix_url=SITE_DATA["API"]["prefixes"]["agencyTiles"]
)

pdf = PdfHandler()

if __name__ == "__main__":
    agencies = parser.agencies()

    agency_invest = parser.agency(
        agency_id=random.choice(
            [agency["agencyCode"] for agency in agencies]
        )
    )

    xlsx_ = XlsxHandler(
        agencies=agencies,
        investments=agency_invest
    )

    xlsx_.write_to_xlsx()
    parser.download_pdf_files(invests_=agency_invest)
    parsed_pdf_data = pdf.parsing_pdf()

    [
        print(
            f'The agency name {item_pdf.get("nameAgency")} from the file does not '
            f'match the agency name from the table {agency_item.get("investmentTitle")} '
            f'UII: {agency_item.get("UII")}'
        )
        if item_pdf.get("UII") == agency_item.get("UII")
        and item_pdf.get("nameAgency") != agency_item.get("investmentTitle")
        else item_pdf
        for item_pdf in parsed_pdf_data
        for agency_item in agency_invest
    ]
