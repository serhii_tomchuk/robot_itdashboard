from typing import List, Union

import requests


class DashboardHandler:
    def __init__(
            self,
            base_url,
            prefix_base_url,
            base_api_url,
            agencies_prefix_url
    ):
        self.base_url = base_url
        self.prefix_base_url = prefix_base_url
        self.base_api_url = base_api_url
        self.requests = requests
        self.session = self.requests.Session()
        self.agencies_prefix_url = agencies_prefix_url

    def _add_token(self):
        try:
            return self._request(self.base_url).headers
        except AttributeError as e:
            print(e)

    def agencies(self) -> List[dict]:
        self.session.headers.update({'Referer': f'{self.base_url}{self.prefix_base_url}'})
        self._add_token()
        data = self._request(
            f'{self.base_url}'
            f'{self.base_api_url}'
            f'{self.agencies_prefix_url}'
        ).json()
        return data["result"]

    def agency(self, agency_id: str) -> list:
        self.session.headers.update(
            {
                'Referer': f'{self.base_url}'
                           f'{self.prefix_base_url}'
                           f'/summary/{agency_id}'
            }
        )

        self._add_token()

        data = self._request(
            f'{self.base_url}'
            f'{self.base_api_url}'
            f'/visualization/agency/investmentsTable/agencyCode/'
            f'{agency_id}?full=1'
        ).json()
        return data["result"]

    def download_pdf_files(self, invests_: list):
        try:
            for item in invests_:
                path = f'./output/{item["UII"]}.pdf'
                self.session.headers.update(
                    {'Referer': f'{self.base_url}/'
                                f'drupal/summary/'
                                f'{item["UII"][:3]}/'
                                f'{item["UII"]}'}
                )
                self._add_token()
                file = self._request(url=f'{self.base_url}'
                                         f'{self.base_api_url}'
                                         f'businesscase/pdf/generate/uii/'
                                         f'{item["UII"]}')

                if file.status_code == 200:
                    print(f"Status code is {file.status_code}!\n"
                          f"Downloading {item['UII']}\n")
                    with open(path, 'wb') as f:
                        f.write(file.content)
                else:
                    print(f"Status code is {file.status_code}!")
                    pass
        except TypeError as e:
            print(e)

    def _request(self, url: str) -> Union[requests.Response, ValueError]:
        try:
            res = self.session.get(url)
            return res
        except Exception as e:
            print(e)
