import os

import openpyxl
import pandas as pd


class XlsxHandler:
    FOLDER_NAME = "/output"
    FILENAME = "file.xlsx"
    PATH = f"./{FOLDER_NAME}/{FILENAME}"

    def __init__(self, agencies, investments):
        self.agencies = agencies
        self.investments = investments

    def _create_folder_and_file(self):
        os.system(f"mkdir .{self.FOLDER_NAME}")
        os.system(
            f"touch "
            f".{self.FOLDER_NAME}/"
            f"{self.FILENAME}"
        )

    def write_to_xlsx(self):

        self._create_folder_and_file()

        writer = pd.ExcelWriter(self.PATH, engine='openpyxl')
        df1 = pd.DataFrame.from_dict(self.agencies)
        df2 = pd.DataFrame.from_dict(self.investments)

        df1.to_excel(
            writer,
            sheet_name='Agencies',
            index=False,
            columns=['agencyName', 'totalSpendingCY']
        )
        df2.to_excel(
            writer,
            sheet_name='Individual Investments',
            index=False,
        )

        writer.save()

