SITE_DATA = {
    "BASE_URL": "https://itdashboard.gov/",
    "prefix_base": "drupal/",
    "API": {
        "BASE_API": "api/v1/ITDB2/",
        "prefixes": {
            "agencyTiles": "visualization/govwide/agencyTiles",
        }
    },

}
