import os

from PyPDF2 import PdfFileReader


class PdfHandler:
    path = './output'

    def _find_pdf(self):
        all_files = os.listdir(f"{self.path}")
        return list(filter(lambda f: f.endswith('.pdf'), all_files))

    def parsing_pdf(self):
        contents = []

        for file in self._find_pdf():
            try:
                with open(f"{self.path}/{file}", 'rb') as f:
                    pdf = PdfFileReader(f)
                    for page_num in range(pdf.getNumPages()):
                        page = pdf.getPage(0)

                    preparing_data = page.extractText().split(
                        "1. Name of this Investment:"
                    )[1].split(
                        "2. Unique Investment Identifier (UII):"
                    )

                    contents.append({
                        "nameAgency": preparing_data[0].replace("\n", "").strip(),
                        "UII": preparing_data[1].replace("\n", "").strip()[:13]
                    })
            except Exception as e:
                print(e)
                pass
        return contents
